import { Meteor } from 'meteor/meteor';
import { Tracker } from 'meteor/tracker';

export default class Security {
    static userIsLoggedIn() {
        return new Promise((resolve) => {
            Tracker.autorun((computation) => {
                Meteor.user() !== undefined &&
                    Meteor.roleAssignment.find().count() &&
                    computation.stop();
                Meteor.user() === null && resolve(false);
                Meteor.user() && resolve(true);
            });
        });
    }
}
