import { RouterFactory } from 'meteor/akryum:vue-router2';

import Security from '../../security/security';

import LoginForm from './components/LoginForm.vue';

const beforeEnter = (to, from, next) => {
    return Security.userIsLoggedIn().then((response) => {
        return response ? next('/videos') : next();
    });
};

const routes = [
    {
        path: '/login',
        name: 'login',
        props: true,
        components: {
            default: LoginForm
        },
        beforeEnter
    }
];

RouterFactory.configure((factory) => {
    factory.addRoutes(routes);
});
