class CRUDService {
    constructor({ collection }) {
        this.collection = collection;
    }
    find(query = {}, projection) {
        return this.collection.find(query, projection);
    }
    findOne(query = {}, projection) {
        return this.collection.findOne(query, projection);
    }
    insert(doc) {
        return this.collection.insert(doc);
    }
    update(query, update, options) {
        return this.collection.update(query, update, options);
    }
    remove(query, options) {
        return this.collection.remove(query, options);
    }
}

export default CRUDService;
