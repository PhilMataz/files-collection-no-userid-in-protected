import VideoService from '../service';

Meteor.publish('videos', function() {
    return VideoService.find({}).cursor;
});

Meteor.publish('video', function(videoId) {
    return VideoService.find({ _id: videoId }).cursor;
});
