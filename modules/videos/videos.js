/* istanbul ignore file */
import { Meteor } from 'meteor/meteor';
import { FilesCollection } from 'meteor/ostrio:files';

const Videos = new FilesCollection({
    collectionName: 'videos',
    debug: false,
    allowClientCode: false, // Disallow remove files from Client
    storagePath: `${Meteor.settings.storagePath}videos`,
    protected() {
        console.log(this.userId);
        return !!this.userId;
    }
});

Videos.collection.deny({
    insert() {
        return true;
    },
    update() {
        return true;
    },
    remove() {
        return true;
    }
});

export { Videos };
