import { Videos } from './videos';

import CRUDService from '../crud/service';

class Service extends CRUDService {
    constructor({ collection }) {
        super({ collection });
    }
    link(video) {
        return this.collection.link(video);
    }
}

export default new Service({
    collection: Videos
});

export { Service as VideoServiceModel };
