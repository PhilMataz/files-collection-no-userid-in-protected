import { RouterFactory } from 'meteor/akryum:vue-router2';

import VideoView from './components/VideoView.vue';

import VideoAdd from './components/VideoAdd.vue';

import Videos from './components/Videos.vue';

const routes = [
    {
        path: '/',
        name: 'videos',
        component: Videos,
        meta: {
            permission: true
        }
    },
    {
        path: '/videos/upload',
        name: 'video.upload',
        component: VideoAdd,
        meta: {
            permission: true
        }
    },
    {
        path: '/video/:videoId',
        name: 'video',
        component: VideoView,
        meta: {
            permission: true
        },
        props: true
    }
];

RouterFactory.configure((factory) => {
    factory.addRoutes(routes);
});
