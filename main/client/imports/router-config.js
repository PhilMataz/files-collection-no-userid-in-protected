import { RouterFactory, nativeScrollBehavior } from 'meteor/akryum:vue-router2';

import './routes';

import Security from '../../../modules/security/security';

const routerFactory = new RouterFactory({
    mode: 'history',
    scrollBehavior: nativeScrollBehavior,
    linkActiveClass: 'active'
});

const router = routerFactory.create();

router.beforeEach((to, from, next) => {
    if (to.meta.permission === false) {
        return next();
    }
    Security.userIsLoggedIn().then((response) => {
        if (
            response ||
            (!response &&
                (!to.meta.permission ||
                    !to.matched[0] ||
                    !to.matched[0].meta.permission))
        ) {
            return next();
        }
        return next('/login');
    });
});

export default router;
