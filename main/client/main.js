import Vue from 'vue';

import { Meteor } from 'meteor/meteor';

import './imports/popper';

import App from './components/App.vue';
// Methods

// Routes
import './imports/routes';

// Style
import './style/styles';

// Vue
import './imports/vue-config';
import router from './imports/router-config';

// Material-Dashboard JS
import './imports/material-dashboard-master/assets/js/core/bootstrap-material-design.min.js';
import './imports/material-dashboard-master/assets/js/plugins/perfect-scrollbar.jquery.min.js';
import './imports/material-dashboard-master/assets/js/material-dashboard.js';
import './imports/material-dashboard-master/assets/js/plugins/jasny-bootstrap.min.js';
import 'bootstrap-select/dist/js/bootstrap-select.js';
import './imports/material-dashboard-master/assets/js/plugins/chartist.min.js';

const vm = new Vue({
    router,
    ...App
}).$mount('#app');

Meteor.startup(() => {});

export default vm;
