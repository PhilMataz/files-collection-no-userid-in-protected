import { Meteor } from 'meteor/meteor';

import './imports/publications';

Meteor.startup(() => {
    if (Meteor.users.find({}).count() === 0) {
        Accounts.createUser({
            username: 'test',
            email: 'test@mail.com',
            password: 'test1234'
        });
    }
});
